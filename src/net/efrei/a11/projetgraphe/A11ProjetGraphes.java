package net.efrei.a11.projetgraphe;

import java.io.File;
import java.util.Scanner;

public class A11ProjetGraphes {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String fileName;
        do {
            System.out.println("==============\nNom du fichier contenant le graphe: ");
            fileName = scanner.nextLine();
            if (!fileName.isEmpty()) {
                File f = new File(fileName);
                if (f != null && f.getName().endsWith(".csv")) {
                    System.out.println("==============\nFichier: " + f.getName());
                    parseGraph(f);
                } else {
                    System.out.println("Fichier introuvable ou extension incorrecte (.csv) !");
                }
            }
        }
        while (!fileName.isEmpty());
        System.out.println("Aucun choix de fichier. Fin du programme.");
    }

    /**
     * Exécute le code demandé pour un graph en particulier
     *
     * @param file Fichier Graphe
     */
    private static void parseGraph(File file) {
        try {
            // Lire le graphe et le stocker en mémoire
            A11Graph graphe = A11Graph.fromCSV(file);
            System.out.println(graphe);
            // Afficher les matrices correspondant au graphe
            System.out.println(graphe.adjacentMatrixToString());
            System.out.println(graphe.valueMatrixToString());
            // Détecter si il y a un circuit ou non
            if (graphe.hasCircuit(true)) {
                System.out.println("Le graphe contient au moins un circuit.");
            } else { // Si il n'y a pas de circuit
                System.out.println("Le graphe ne possède aucun circuit.");
                // Calcul des rangs et affichage
                System.out.println(graphe.calculRangsToString());
                // Si le graphe est un graphe d'ordonnancement
                if (graphe.estOrdonnancement()){
                    System.out.println("C'est un graphe d'ordonnancement.");
                    graphe.calculCalendrier();
                } else {
                    System.out.println("Ce n'est pas un graphe d'ordonnancement.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
