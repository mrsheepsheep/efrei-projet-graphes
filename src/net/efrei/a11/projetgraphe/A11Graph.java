package net.efrei.a11.projetgraphe;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class A11Graph {

    private int[][] graph;
    private int[][] adjacentMatrix;
    private Integer[][] valueMatrix;

    private int sommets;
    private int arcs;

    public A11Graph(int[][] graph) {
        this.graph = graph;
        this.arcs = graph.length;
        this.sommets = calculSommets();
        generateAdjacentMatrix();
    }

    @Override
    public String toString() {
        String toPrint = String.format("Graphe: %s arcs - %s sommets\n", arcs, sommets);
        for (int noLigne = 0; noLigne < arcs; noLigne++) {
            int[] arc = graph[noLigne];
            toPrint += String.format("%s => %s (%s)\n", arc[0], arc[1], arc[2]);
        }
        return toPrint;
    }

    /**
     * Calcule le nombre de sommets à partir de la matrice d'arcs.
     * Compte simplement le nombre d'arcs ayant un sommet inconnu qu'il soit en origine ou en successeur
     * En partant du principe que les arcs vont en s'incrémentant,
     * on ne gardera que l'arc d'indice maximal à chaque itération.
     *
     * @return le nombre de sommets
     */
    private int calculSommets() {
        int sommets = graph[0][0];
        for (int[] arc : graph) {
            if (sommets < arc[0])
                sommets = arc[0];
            if (sommets < arc[1])
                sommets = arc[1];
        }
        return sommets + 1; // On ajoute 1 car on commence à l'indice 0
    }

    /**
     * Génère la matrice adjacente basée sur la matrice initiale
     * Doit aussi générérer la matrice de valeurs associées
     */
    private void generateAdjacentMatrix() {
        adjacentMatrix = new int[sommets][sommets];
        valueMatrix = new Integer[sommets][sommets];
        for (int[] arc : graph) {
            adjacentMatrix[arc[0]][arc[1]] = 1;
            valueMatrix[arc[0]][arc[1]] = arc[2];
        }
    }

    /**
     * Crée un String affichant la matrice adjacente (si non nulle)
     * Doit inclure les numéros de lignes et colonnes
     *
     * @return
     */
    public String adjacentMatrixToString() {
        if (adjacentMatrix == null) {
            generateAdjacentMatrix();
        }
        String toPrint = "Matrice d'adjacence:\n     "; // Ajout de 5 espaces pour la suite
        // Affiche la première ligne avec les numéros de sommets
        for (int x = 0; x < adjacentMatrix.length; x++) {
            toPrint += String.format("%5s", x);
        }
        toPrint += "\n";
        for (int x = 0; x < adjacentMatrix.length; x++) {
            toPrint += String.format("%5s", x);
            for (int y = 0; y < adjacentMatrix.length; y++) {
                toPrint += String.format("%5s", adjacentMatrix[x][y]);
            }
            toPrint += "\n";
        }
        return toPrint;
    }

    /**
     * Code très similaire à la fonction précédente, adapté à la matrice de valeurs
     *
     * @return
     */
    public String valueMatrixToString() {
        if (valueMatrix == null) {
            generateAdjacentMatrix();
        }
        String toPrint = "Valeur des arcs:\n     ";
        for (int x = 0; x < valueMatrix.length; x++) {
            toPrint += String.format("%5s", x);
        }
        toPrint += "\n";
        for (int x = 0; x < valueMatrix.length; x++) {
            toPrint += String.format("%5s", x);
            for (int y = 0; y < valueMatrix.length; y++) {
                toPrint += String.format("%5s", (valueMatrix[x][y] != null ? valueMatrix[x][y] : "*"));
            }
            toPrint += "\n";
        }
        return toPrint;
    }

    /**
     * Convertit un File au format CSV donné en objet A11Graph
     *
     * @param csvFile Fichier à charger
     * @return objet A11Graph
     * @throws IOException           Si le fichier n'est pas au format .CSV
     * @throws NumberFormatException Si une valeur n'est pas un nombre
     */
    public static A11Graph fromCSV(File csvFile) throws IOException, NumberFormatException {
        // Erreur si pas un CSV
        if (!csvFile.getName().endsWith(".csv")) {
            throw new IOException("File is not a CSV file");
        }
        // Crée une matrice d'entiers de taille dynamique
        List<int[]> matrix = new ArrayList<>();

        // Lecture du fichier
        BufferedReader reader = new BufferedReader(new FileReader(csvFile));
        String readLine;
        while ((readLine = reader.readLine()) != null) {
            // Suppression des espaces inutiles et séparation par virgules
            String[] values = readLine.replaceAll(" ", "").split(";");
            // Création d'un tableau d'entiers de même taille
            int[] intValues = new int[values.length];
            // Conversion de chaque valeur et ajout dans le tableau
            for (int i = 0; i < intValues.length; i++) {
                String value = values[i];
                // Conversion
                try {
                    Integer intValue = Integer.parseInt(value);
                    intValues[i] = intValue;
                } catch (NumberFormatException e) {
                    throw new IOException("Error parsing values ! " + value + " is not an int.");
                }
            }
            // Ajout des données de la ligne dans la matrice
            matrix.add(intValues);
        }

        // Conversion de la matrice dynamique en une matrice fixe
        int[][] intMatrix = matrix.toArray(new int[matrix.size()][]);
        return new A11Graph(intMatrix);
    }


    /**
     * Vérifie si le graph possède des circuits
     * Méthode: élimination des points de sortie
     * Pour représenter la suppression d'un sommet,
     * toutes les valeurs dans la matrice d'adjacence
     * passeront à -1
     *
     * @param log affiche ou non les messages
     * @return true si au moins un circuit existe
     */
    public boolean hasCircuit(boolean log) {
        if (log) System.out.println("== Vérification de la présence de circuit:");
        // On clone la matrice adjacente pour éviter d'y appliquer des modifications
        int[][] adjacentMatrix = this.adjacentMatrix.clone();
        List<Integer> removed = new ArrayList<>(); // Contient tous les sommets supprimés
        do {
            List<Integer> sorties = new ArrayList<>(); // Contient les sorties trouvées lors de la dernière recherche
            for (int i = 0; i < adjacentMatrix.length; i++) {
                // On vérifie que le sommet n'est pas supprimé
                if (!removed.contains(i)) {
                    int j;
                    for (j = 0; j < adjacentMatrix[i].length; j++) {
                        // Ce sommet est une sortie si il ne possède aucun successeur
                        // Donc si il n'y a aucun 1 sur la ligne qui lui correspond dans la
                        // matrice d'adjacence.
                        // On vérifie aussi que ce sommet n'est pas déjà supprimé
                        if (adjacentMatrix[i][j] == 1 && !removed.contains(j)) {
                            break; // Il y a un 1 sur la ligne, on passe au suivant car ce n'est pas une sortie
                        }
                    }
                    if (j == adjacentMatrix[i].length) { // Si on arrive ici, c'est que la boucle for (j...) s'est entièrement exécutée
                        sorties.add(i);
                        // Suppression du sommet
                        removed.add(i);
                    }
                }
            }
            if (log) {
                String toPrint = "Sorties: [ ";
                for (int sortie : sorties) {
                    toPrint += sortie + " ";
                }
                System.out.println(toPrint + "]");
            }
            if (sorties.size() == 0) {
                return true;
            }
        } while (removed.size() != sommets); // On répète jusqu'à ce que tous les sommets soient supprimés
        return false;
    }

    /**
     * Calcule les rangs des sommets si le graphe n'a pas de circuit.
     * Méthode utilisée: suppression des entrées
     *
     * @return la liste des sommets -> leur rang. null si il y a un circuit.
     */
    public int[] calculRangs(boolean log) {
        if (log) System.out.println("== Calcul des rangs:");
        int[][] adjacentMatrix = this.adjacentMatrix.clone();
        int[] rangs = new int[adjacentMatrix.length];
        List<Integer> entrees = new ArrayList<>();
        List<Integer> removed = new ArrayList<>();
        int k = 0; // Rang actuel
        do {
            if (log) System.out.println("Rang actuel: " + k);
            entrees.clear();
            for (int i = 0; i < adjacentMatrix.length; i++) { // On sélectionne un sommet
                if (!removed.contains(i)) {
                    int nbPred = 0;
                    for (int j = 0; j < adjacentMatrix.length; j++) { // Pour chaque ligne
                        // Seule différence avec hasCircuit - On vérifie sur les colonnes
                        // D'où l'inversement de j et i
                        if (!removed.contains(j)) {
                            nbPred += adjacentMatrix[j][i];
                        }
                    }
                    if (nbPred == 0) {
                        entrees.add(i);
                        rangs[i] = k;
                        // On ne peut pas supprimer l'entrée ici, autrement les sommets suivants pourraient être
                        // considérés comme des entrées, dans le calcul de rang actuel.
                    }
                }
            }
            if (log) {
                String toPrint = "Entrées: [ ";
                for (int entree : entrees) {
                    toPrint += entree + " ";
                }
                System.out.println(toPrint + "]");
            }
            k++; // Rang suivant
            removed.addAll(entrees);
            // On ajoute toutes les entrées trouvées aux entrées à supprimer.
        } while (entrees.size() != 0);
        return rangs;
    }

    public String calculRangsToString() {
        String toPrintSommet = "Sommet: ";
        String toPrintRang = "Rang:   ";
        int[] rangs = calculRangs(true);
        for (int i = 0; i < rangs.length; i++) {
            toPrintSommet += String.format("%5s", i);
            toPrintRang += String.format("%5s", rangs[i]);
        }
        return toPrintSommet + "\n" + toPrintRang;
    }

    public boolean estOrdonnancement() {
        System.out.println("== Vérification de l'ordonnancement:");
        // Circuit ?
        if (hasCircuit(false)) {
            System.out.println("Il y a un circuit !");
            return false;
        }
        // 1 entrée et 1 sortie ?
        Integer entree = null;
        Integer sortie = null;
        for (int i = 0; i < adjacentMatrix.length; i++) {
            int nbPredecesseurs = 0;
            int nbSuccesseurs = 0;

            for (int j = 0; j < adjacentMatrix.length; j++) {
                nbPredecesseurs += adjacentMatrix[j][i];
                nbSuccesseurs += adjacentMatrix[i][j];

                // Au passage, on vérifie si la valeur n'est pas négative.
                if (valueMatrix[i][j] != null && valueMatrix[i][j] < 0) {
                    System.out.println("Un arc est à valeur négative !");
                    return false;
                }
            }
            if (nbPredecesseurs == 0) {
                // Si il y a déjà une entrée détectée, alors on s'arrête là
                if (entree != null) {
                    System.out.println("Il y a plusieurs entrées !");
                    return false;
                }
                entree = i;
            }
            if (nbSuccesseurs == 0) {
                if (sortie != null) {
                    System.out.println("Il y a plusieurs sorties !");
                    return false;
                }
                sortie = i;
            }
        }
        // On connaît désormais notre entrée et notre sortie.
        // On vérifie les valeurs de l'entrée
        for (Integer v : valueMatrix[entree]) {
            if (v != null && v != 0) {
                return false;
            }
        }
        // L'entrée est bonne.
        // On vérifie si tous les arcs vers l'extérieur d'un même sommet ont la même valeur
        for (int i = 0; i < valueMatrix.length; i++) {
            Integer valeur = valueMatrix[i][0]; // Valeur de référence initiale. Si c'est null, on corrigera après.
            for (int j = 0; j < valueMatrix[i].length; j++) {
                Integer valeurActuelle = valueMatrix[i][j]; // Valeur de l'arc actuel
                if (valeur == null) { // Initialisation de la valeur de référence
                    valeur = valeurActuelle;
                } else if (valeurActuelle != null && valeurActuelle != valeur) {
                    // Si un arc non nul n'a pas la même valeur
                    System.out.println("Un sommet n'a pas tous ses arcs de même valeur: " + i);
                    return false;
                }
            }
        }
        // C'est un graphe d'ordonnancement !
        return true;
    }

    public void calculCalendrier() {
        // Conversion de la liste des sommets => rangs en rangs => sommets
        int[] sommetsRangs = calculRangs(false);
        List<List<Integer>> rangsSommets = new ArrayList<>();
        for (int sommet = 0; sommet < sommetsRangs.length; sommet++) {
            int rang = sommetsRangs[sommet];
            try {
                rangsSommets.get(rang).add(sommet);
            } catch (IndexOutOfBoundsException e) {
                List<Integer> a = new ArrayList<>();
                a.add(sommet);
                rangsSommets.add(a);
            }
        }

        /*
        CALCUL DES DATES AU PLUS TOT
         */
        System.out.println("== Calcul des dates au plus tôt:");
        int[] datesAuPlusTot = new int[this.sommets];
        datesAuPlusTot[0] = 0;
        // Iteration sur les rangs pour calculer la date au plus tôt
        for (int rang = 1; rang < rangsSommets.size(); rang++) {
            List<Integer> sommets = rangsSommets.get(rang);
            for (int sommet : sommets) {
                System.out.println(String.format("[r%s:s%s] Sommet suivant", rang, sommet));
                int dateAuPlusTot = 0; // Date au plus tot trouvée dans l'itération actuelle
                int predecesseurAuPlusTot = 0; // Prédecesseur correspondant
                for (int predecesseur = 0; predecesseur < this.sommets; predecesseur++) { // Pour chaque potentiel prédecesseur
                    // Si c'est un prédécesseur
                    if (valueMatrix[predecesseur][sommet] != null) {
                        int longueurPredecesseur = valueMatrix[predecesseur][sommet];
                        // Si c'est un prédecesseur, il a forcément déjà une date au plus tôt définie
                        int predecesseurDateAuPlusTot = datesAuPlusTot[predecesseur];
                        // La date calculée sera donc la date au plus tôt du prédécesseur + sa longueur
                        int date = longueurPredecesseur + predecesseurDateAuPlusTot;
                        // Si cette date est plus grande que celle déjà trouvée précédemment
                        if (date > dateAuPlusTot) {
                            System.out.println(String.format("[r%s:s%s] Le prédecesseur %s modifie la date au plus tôt (%s)", rang, sommet, predecesseur, date));
                            // C'est donc celle ci qu'il faut garder
                            dateAuPlusTot = date;
                            predecesseurAuPlusTot = predecesseur;
                        }
                    }
                }
                // A partir de cette étape, on connaît le predecesseur au plus tôt ainsi que la date au plus tôt
                System.out.println(String.format("[r%s:s%s] Date au plus tôt: %s (%s)", rang, sommet, dateAuPlusTot, predecesseurAuPlusTot));
                datesAuPlusTot[sommet] = dateAuPlusTot;
            }
        }

        /*
        CALCUL DES DATES AU PLUS TARD
         */
        System.out.println("== Calcul des dates au plus tard:");
        int[] datesAuPlusTard = new int[this.sommets];
        // La dernière date au plus tard est la dernière date au plus tôt
        datesAuPlusTard[this.sommets - 1] = datesAuPlusTot[this.sommets - 1];
        // On parcourt sur les rangs décroissants
        for (int rang = rangsSommets.size() - 1; rang >= 0; rang--) {
            List<Integer> sommets = rangsSommets.get(rang);
            for (int sommet : sommets) {
                System.out.println(String.format("[r%s:s%s] Sommet suivant", rang, sommet));
                int dateAuPlusTard = datesAuPlusTard[this.sommets - 1];
                int successeurAuPlusTard = this.sommets - 1;
                for (int successeur = 0; successeur < this.sommets; successeur++) {
                    // Si c'est un successeur
                    if (valueMatrix[sommet][successeur] != null) {
                        int longueurSuccesseur = valueMatrix[sommet][successeur];
                        int successeurDateAuPlusTard = datesAuPlusTard[successeur];
                        int date = successeurDateAuPlusTard - longueurSuccesseur; // Soustraction
                        // On cherche ici le minimum des dates pour avoir celle au plus tard
                        if (date < dateAuPlusTard) {
                            System.out.println(String.format("[r%s:s%s] Le successeur %s modifie la date au plus tard (%s)", rang, sommet, successeur, date));
                            dateAuPlusTard = date;
                            successeurAuPlusTard = successeur;
                        }
                    }
                }
                // A partir de cette étape, on connaît le successeur au plus tard ainsi que la date au plus tard
                System.out.println(String.format("[r%s:s%s] Date au plus tard: %s (%s)", rang, sommet, dateAuPlusTard, successeurAuPlusTard));
                datesAuPlusTard[sommet] = dateAuPlusTard;
            }
        }

        /*
        CALCUL DES MARGES
         */
        int[] margeTotale = new int[this.sommets];
        for (int sommet = 0; sommet < this.sommets; sommet++) {
            margeTotale[sommet] = datesAuPlusTard[sommet] - datesAuPlusTot[sommet];
        }

        /*
        AFFICHAGE
         */
        String toPrint = "== Calendrier final:";
        String toPrintRangs = "Rang              |";
        String toPrintTache = "Tâche             |";
        String toPrintLongueur = "Longueur          |";
        String toPrintDatePlusTot = "Date au plus tôt  |";
        String toPrintDatePlusTard = "Date au plus tard |";
        String toPrintMarge = "Marge totale      |";
        for (int rang = 0; rang < rangsSommets.size(); rang++) {
            for (int sommet : rangsSommets.get(rang)) {
                toPrintRangs += String.format("%5s|", rang);
                toPrintTache += String.format("%5s|", sommet);
                toPrintLongueur += String.format("%5s|", graph[sommet][2]);
                toPrintDatePlusTot += String.format("%5s|", datesAuPlusTot[sommet]);
                toPrintDatePlusTard += String.format("%5s|", datesAuPlusTard[sommet]);
                toPrintMarge += String.format("%5s|", margeTotale[sommet]);
            }
        }
        System.out.println(toPrint);
        System.out.println(toPrintRangs);
        System.out.println(toPrintTache);
        System.out.println(toPrintLongueur);
        System.out.println(toPrintDatePlusTot);
        System.out.println(toPrintDatePlusTard);
        System.out.println(toPrintMarge);

    }

}
